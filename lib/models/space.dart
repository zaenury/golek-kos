class Space {
  int id;
  String name;
  int price;
  String imageUrl;
  String city;
  String country;
  int rating;

  Space(
      {this.id,
      this.name,
      this.price,
      this.imageUrl,
      this.city,
      this.country,
      this.rating});
}
